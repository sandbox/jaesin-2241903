<?php
/**
 * Admin pages for the pagedown module.
 */

/**
 * Form to configure which formats should use the Pagedown previewer.
 *
 * @param array $form
 * @param $form_state
 * @return mixed
 * @throws Exception
 * @throws PDOException
 */
function pagedown_admin_configure_form($form = array(), &$form_state) {
  // Get the filter formats and make an options array out of them
  $options = array();
  foreach(filter_formats() as $key => $value) {
    $options[$key] = $value->name;
  }
  $form['pagedown_filter_formats'] = array(
    '#type' => 'select',
    '#title' => t('Pagedown enabled text formats'),
    '#description' => t('Select which filter formats to enable the previewer in.'),
    '#default_value' => !empty($form_state['values']['pagedown_filter_formats'])
      ? $form_state['values']['pagedown_filter_formats']
      : variable_get('pagedown_filter_formats', array()),
    '#options' => $options,
    '#multiple' => TRUE,
  );
  $form['pagedown_default_position'] = array(
    '#type' => 'select',
    '#title' => t('Default Split Pane Position'),
    '#description' => t('the preview will split the editor window in a split view. Select where the preview pane should be located'),
    '#default_value' => !empty($form_state['values']['pagedown_default_position'])
      ? $form_state['values']['pagedown_default_position']
      : variable_get('pagedown_default_position', 'right'),
    '#options' => array(
      'right' => t('Right'),
      'left' => t('Left'),
      'top' => t('Top'),
      'bottom' => t('Bottom'),
    ),
  );

	return system_settings_form($form);
}