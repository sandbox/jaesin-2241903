PAGEDOWN
=========

This is a tool for previewing markdown as you type. It is **NOT** intended to be any sort of WYSIWYG. One feature of markdown syntax is that you can compose content quickly without taking your hands off the keyboard. This module provides a split pane or tab for previewing markdown. Markdown combined with a decent style guide (trust your style guide), should have you composing presentable content easily and efficiently.

Other Requirements
-----------------------
The [Libraries API](https://drupal.org/project/libraries) is required. This module does not contain a text filter. The [Markdown Filter](https://drupal.org/project/markdown) module can handle the final processing for you.

A Markdown previewer for drupal filter formats (Requires Markdown)

A markdown previewer for the markdown filter. Requires the markdown module, the libraries api as well as the pagedown js from http://code.google.com/p/pagedown/source/checkout. A git version may be available here: https://github.com/ujifgc but I have not verified the copy.

Currently works with the pagedown [library](http://code.google.com/p/pagedown/source/checkout).

I would like to use a wrapper so we could also use the [marked](https://github.com/chjj/marked) library. That would give users a choice and let us compare the performance of both systems.

Version
--------
0.01b

Tech
--------
