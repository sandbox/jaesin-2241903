<?php

/**
* Implements hook_libraries_info.
*
* Define the pagedown js library
*/
function pagedown_libraries_info() {
  $libraries['pagedown'] = array(
    'name' => 'Pagedown',
    'vendor url' => 'http://code.google.com/p/pagedown/wiki/PageDown',
    'download url' => 'http://code.google.com/p/pagedown/source/checkout',
    'version arguments' => array(
      'file' => 'package.json', // Could be any file with version info
      'pattern' => '/.*version.*"([0-9\.]+)/', //"version": "1.1.0"
      'lines' => 3,
    ),
    'files' => array(
      'js' => array('Markdown.Converter.js', 'Markdown.Sanitizer.js'),
    ),
  );
  return $libraries;
}

/**
 * Implements hook_form_alter
 */
function pagedown_form_alter(&$form, &$form_state) {
  $formats = variable_get('pagedown_filter_formats', array());
  if (!empty($formats)) {
    if (isset($form['body'])) {
      pagedown_include_pagedown();
    } else {
      // Drupal 7 (php v5.25) compatible version of array_walk.
      $elements = element_children($form);
      array_walk_recursive($elements, create_function('$value, $key', 'if ($value["#type"]=="text_format") { pagedown_include_pagedown(); return; }'));
    }
  }
}

/**
 * Implements hook_form_menu
 */
function pagedown_menu() {
  $items = array();
  $items['admin/config/content/pagedown'] = array(
    'title' => 'Pagedown Configuration',
    'description' => 'Configure which text formats will employ the Pagedown previewer.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pagedown_admin_configure_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => "pagedown.admin.inc",
  );
  return $items;
}

function pagedown_include_pagedown() {
  // Add the pagedown library.
  libraries_load('pagedown');
  // Add our integration js.
  drupal_add_js(drupal_get_path('module', 'pagedown').'/js/pagedown.js');
  drupal_add_css(drupal_get_path('module', 'pagedown').'/css/pagedown.css');
  // Create and add the filter formats the previewer should be attached to.
  $settings = array(
    'pagedown' => array(
      'filter_formats' => array_keys(variable_get('pagedown_filter_formats', array())),
      'default_position' => variable_get('pagedown_default_position', 'right'),
    ),
  );
  drupal_add_js($settings, 'setting');
}