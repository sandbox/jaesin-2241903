/**
 * Created by jaesin on 04/16/14.
 */

(function($) {
  Drupal.behaviors.pagedown = {
    attach: function (context, settings) {
      // make sure the filter_formats array exists.
      settings.pagedown = settings.pagedown || {
        filter_formats : [],
        default_position : 'right'
      }

      $('.filter-list.form-select').change(function(){

        // Get the textarea for the given format selector.
        var editorSelector = pagedownGetTextareaFromFilterSelector($(this).attr('id'));
        var editor = $(editorSelector).first();
        var position = editor.data('pagedown_position') || settings.pagedown.default_position;
        // Get the previewer.
        var viewer = $(editorSelector + '-preview');
        // Make sure there is only one viewer for any given editor.
        if (viewer.length != 1) {
          viewer.remove();
          viewer = $('<div id="'+editor.attr('id') + '-preview" class="pagedown-preview enabled"></div>');
        }

        // Define the key press responder.
        function editorKeyUp(e) {
          viewer.html(converter.makeHtml(editor.val()));
        }

        // @todo refactor
        // When the previewer is less than 20%, switch to horizontally split preview.
        // Add a vertical grippie (resize disabled).
        // document this file better.
        // Add additional support for marked: https://github.com/chjj/marked.
        // Add syncronized scrolling
        if($.inArray($(this).val(), settings.pagedown.filter_formats) >= 0) {

          editor.parent().addClass('pagedown-textarea-wrapper');
          // Add resize handler depending on position.
          pagedownModifyResizeHandlerByPosition(position, 'add', editor, viewer);

          // Get the converter and convert initial value.
          var converter = window.Markdown.getSanitizingConverter();
          viewer.html(converter.makeHtml(editor.val()));

          // Bind to the callback to process the markdown into previewable html.
          editor.keyup(editorKeyUp);

          // Add the preview enabled class.
          editor.parent().addClass('pagedown-enabled');

        } else {
          pagedownModifyResizeHandlerByPosition(position, 'remove', editor, viewer);
          editor.unbind('keyup', editorKeyUp);
          viewer.remove();
          editor.parent().removeClass('pagedown-enabled');
          editor.width('100%');
        }
      });
    },
  };
  function pagedownGetTextareaFromFilterSelector(filterSelectorId) {
    return '#' + filterSelectorId.substring(0,filterSelectorId.indexOf('-format')) + '-value';
  }
  function pagedownModifyResizeHandlerByPosition (position, action, editor, viewer) {
    switch (position) {
      case 'left':
        pagedownModifyResizeHandlerLeft(action, editor, viewer);
        break;
      case 'top':
        pagedownModifyResizeHandlerTop(action, editor, viewer);
        break;
      case 'bottom':
        pagedownModifyResizeHandlerBottom(action, editor, viewer);
        break;
      default:
        // Add the viewer.
        editor.after(viewer);
        pagedownModifyResizeHandlerRight(action, editor, viewer);
        break;
    }
  }
  function pagedownModifyResizeHandlerRight(action, editor, viewer) {
    // Define some variables for this Scope.
    var staticOffset, staticVOffset;
    // Get the vertical grippie.
    var verticalGrippie = editor.siblings('.vertical-grippie');
    // Make sure there is only one vertical grippie.
    if (verticalGrippie.length != 1) {
      verticalGrippie.remove();
      verticalGrippie = $('<div class="vertical-grippie" />');
    }

    // Define resize functions event handlers for this position.
    function startDrag(e) {
      staticOffset = editor.height() - e.pageY;
      viewer.css('opacity', 0.25);
      $(document).mousemove(performDrag).mouseup(endDrag);
      return false;
    }

    function performDrag(e) {
      var newHeight = editor.outerHeight();
      viewer.height(newHeight - (viewer.outerHeight() - viewer.height()));
      verticalGrippie.height(newHeight - (verticalGrippie.outerHeight() - verticalGrippie.height()));
      verticalGrippie.offset({"top": editor.offset().top, "left":editor.offset().left + editor.width() + 6});
      return false;
    }

    function endDrag(e) {
      $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag);
      viewer.css('opacity', 1);
    }

    // Define resize functions event handlers for this position.
    function startVDrag(e) {
      staticVOffset = editor.width() - e.pageX;
      editor.css('opacity', 0.25);
      viewer.css('opacity', 0.25);
      $(document).mousemove(performVDrag).mouseup(endVDrag);
      return false;
    }

    function performVDrag(e) {
      var newWidth = (Math.min(editor.parent().width() - 50, staticVOffset + e.pageX) + 5) + 'px';
      editor.width(newWidth);
      verticalGrippie.offset({"top": editor.offset().top, "left":editor.offset().left + editor.width() + 6});
      viewer.width(totalWidth - editor.outerWidth(true) -  verticalGrippie.outerWidth(true) - (viewer.outerWidth(true) - viewer.width()));
      return false;
    }

    function endVDrag(e) {
      $(document).unbind('mousemove', performVDrag).unbind('mouseup', endVDrag);
      editor.css('opacity', 1);
      viewer.css('opacity', 1);
    }


    if (action == 'add') {
      // Resize the editor to fit the previewer.
      var totalWidth = editor.parent().width();
      editor.width(Math.round(totalWidth/2) - Math.round(verticalGrippie.outerWidth(true)/2));
      viewer.width(totalWidth - editor.outerWidth(true) -  verticalGrippie.outerWidth(true) - (viewer.outerWidth(true) - viewer.width()));
      viewer.height(editor.height()-6);
      editor.parent().addClass('split-vertical');
      // Add resizeing related event handlers.
      editor.siblings('.grippie').mousedown(startDrag);

      // Add Vertical grippie.
      editor.after(verticalGrippie);
      verticalGrippie.height(editor.outerHeight() - (verticalGrippie.outerHeight(true) - verticalGrippie.height()));
      verticalGrippie.offset({"top": editor.offset().top, "left":editor.offset().left + editor.outerWidth()});
      verticalGrippie.mousedown(startVDrag);

    } else {
      editor.siblings('.grippie').unbind('mousedown', startDrag);
      editor.remove(verticalGrippie);
    }
  }
  function pagedownModifyResizeHandlerLeft(action, editor, viewer) {

  }
  function pagedownModifyResizeHandlerTop(action, editor, viewer) {

  }
  function pagedownModifyResizeHandlerBottom(action, editor, viewer) {

  }
})(jQuery);
